<?php
    /*
    * Plugin Name: Base64 Images Plugin
    * Plugin URI: https://bitbucket.org/MacPrawn/base64images
    * Description: This plugin optionally encodes images on your WordPress site, mainle for SEO benefits. (https://varvy.com/pagespeed/base64-images.html)
    * Version: 1.0
    * Author: Jean Le Clerc
    * Author URI: http://nibnut.com
    * Requires at least: 4.0.0
    * Tested up to: 4.7.1
    *
    * Text Domain: base64images
    * Domain Path: /languages/
    */
    
    if(!defined('ABSPATH')) exit; // Exit if accessed directly
    
	if(!class_exists('TruckEngineCodesPlugin')) {
		class Base64ImagesPlugin {
			static protected $_instance;
			static public function instance(){
				if(is_null(Base64ImagesPlugin::$_instance)) Base64ImagesPlugin::$_instance = new Base64ImagesPlugin();
				return Base64ImagesPlugin::$_instance;
			}
			
			function __construct() {
				add_filter('image_downsize', array($this, 'image_downsize'), 10, 3);
			}
			
			public function image_downsize($downsize, $id, $size) {
                $is_image = wp_attachment_is_image($id);
                if($is_image) {
                    $img_url = ''; //get_post_meta($id, '_base64_image', true);
                    if(!$img_url) {
                        $image_path = get_post_meta($id, '_wp_attached_file', true);
                        if(($uploads = wp_get_upload_dir()) && (false === $uploads['error']) && (0 !== strpos($image_path, $uploads['basedir']))) {
                            if(false !== strpos($image_path, 'wp-content/uploads')) $image_path = trailingslashit($uploads['basedir'].'/'._wp_get_attachment_relative_path($image_path)).basename($image_path);
                            else $image_path = $uploads['basedir'].'/'.$image_path;
                        }
                        if(file_exists($image_path)) {
                            $filetype = wp_check_filetype($image_path);
                            // Read image path, convert to base64 encoding
                            $imageData = base64_encode(file_get_contents($image_path));
                            // Format the image SRC:  data:{mime};base64,{data};
                            $img_url = 'data:image/'.$filetype['ext'].';base64,'.$imageData;
                            //update_post_meta($id, '_base64_image', $img_url);
                        }
                    }
                    
                    if($img_url) {
                        $width = $height = 0;
                        $meta = wp_get_attachment_metadata($id);
                        if(isset($meta['width'], $meta['height'])) {
                            $width = $meta['width'];
                            $height = $meta['height'];
                        }
                    
                        // we have the actual image size, but might need to further constrain it if content_width is narrower
                        list($width, $height) = image_constrain_size_for_editor($width, $height, $size);
                        
                        return array($img_url, $width, $height, false);
                    }
                }
                return false;
			}
		}
		Base64ImagesPlugin::instance();
	}
    
    function Base64ImagesPlugin() {
        return Base64ImagesPlugin::instance();
    }
    add_action('plugins_loaded', 'Base64ImagesPlugin');
?>
